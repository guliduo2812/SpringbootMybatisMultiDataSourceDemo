package com.qianyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* @author 作者zhaopin
* @version 创建时间：2018年10月9日 下午11:13:33
* 类说明
*/
@SpringBootApplication
public class MybatisApp02 {
	public static void main(String[] args) {
		SpringApplication.run(MybatisApp02.class, args);
	}
}
