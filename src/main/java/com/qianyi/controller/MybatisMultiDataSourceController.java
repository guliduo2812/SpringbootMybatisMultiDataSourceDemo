package com.qianyi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qianyi.test01.service.UserService01;
import com.qianyi.test02.service.UserService02;

/**
* @author 作者zhaopin
* @version 创建时间：2018年10月9日 下午11:20:43
* 类说明
*/
@RestController
public class MybatisMultiDataSourceController {
	@Autowired
	private UserService01 userService01;
	
	@Autowired
	private UserService02 userService02;
	
	
	@RequestMapping("/insertTest01User")
	public Integer insertTest01User(String name, Integer age) {
		return userService01.insertUser(name, age);
	}
	
	@RequestMapping("/insertTest02User")
	public Integer inserTest02tUser(String name, Integer age) {
		return userService02.insertUser(name, age);
	}
	
}
