package com.qianyi.entity;

import lombok.Data;

/**
* @author 作者zhaopin
* @version 创建时间：2018年9月26日 下午10:40:48
* 类说明:数据实体类
*/
@Data
public class User {

	private Integer id;
	private String name;
	private Integer age;
	
}
