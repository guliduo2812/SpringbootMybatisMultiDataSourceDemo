package com.qianyi.test01.service;
/**
* @author 作者zhaopin
* @version 创建时间：2018年9月26日 下午11:03:15
* 类说明
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qianyi.test01.mapper.UserMapper01;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class UserService01 {
	@Autowired
	private UserMapper01 userMapperTest01;

	@Transactional(transactionManager = "test1TransactionManager")
	public int insertUser(String name, Integer age) {
		int insertUserResult = userMapperTest01.insert(name, age);
		log.info("######insertUserResult:{}##########", insertUserResult);
		int i = 1 / age;
		// 怎么样验证事务开启成功!~
		return insertUserResult;
	}

}
