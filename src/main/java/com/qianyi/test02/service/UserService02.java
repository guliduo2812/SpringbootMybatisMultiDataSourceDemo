package com.qianyi.test02.service;
/**
* @author 作者zhaopin
* @version 创建时间：2018年9月26日 下午11:03:15
* 类说明
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qianyi.test02.mapper.UserMapper02;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class UserService02 {
	@Autowired
	private UserMapper02 userMapperTest02;

	@Transactional(transactionManager = "test2TransactionManager")
	public int insertUser(String name, Integer age) {
		int insertUserResult = userMapperTest02.insert(name, age);
		log.info("######insertUserResult:{}##########", insertUserResult);
		// 怎么样验证事务开启成功!~
		int i = 1 / age;
		return insertUserResult;
	}

}
